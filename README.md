# Word Sequence Counter
This is a text analyzer than currently supports finding the most frequently occurring 3 word phrases in any text.

## Instructions
Proceed to the project directory in a shell.

Any Ruby version > 2.0.0 will be fine.
Check with `ruby -v`

You may pass one or more text arguments to the program via `.txt` files or raw text. Order does not matter. Example texts can be found under the `/texts` directory.

Example:
`ruby ./solution.rb text/frankenstein.txt some random text text/leviathan.txt`

The program also accepts input on stdin:
`cat text/frankenstein.txt | ./solution.rb`

## Docker instructions
Proceed to the project directory in a shell. Ensure docker is installed by typing `docker`.

Build the image from the included Dockerfile.
`docker build -t text-analyzer .`

Run the image with one or more text arguments
`docker run text-analyzer text/frankenstein.txt some random text text/leviathan.txt`

## Running the tests
`ruby tests/test_text_analyzer.rb`

## Structure
The code is organized into A `TextAnalyzer` class which is primarily responsible for reading text data and printing output. In this class I mixed in the `PhraseHelper` module which handles splitting the text into words/phrases, and counting/sorting those phrases. To add more functionality, we could include another module.

The `TextAnalyzer` class is tested by tests/test_text_analyzer.rb`

We can pass text input to the `TextAnalyzer` via solution.rb which accepts arguments or input on stdin.

## Potential Improvements

 1. Needs to be adapted to accept unicode characters.
 2. Phrase counting could potentially be faster than O(n)
 3. Refactor regex to be more readable.
