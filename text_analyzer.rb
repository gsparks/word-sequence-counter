require_relative 'helpers/phrase_helper'

class TextAnalyzer

    include PhraseHelper

    attr_accessor :raw_text

    def initialize(text_sources)
        @raw_text = read_text(text_sources)
    end

    # Prints out the 100 most used 3 word phrases
    # along with their respective counts
    def print_top_phrases
        get_top_phrases.each do |phrase|
            puts "#{phrase[0]} - #{phrase[1]}"
        end
    end

    private

    # Takes an array of text sources.
    # A text source is a string that either refers to a file or is text itself.
    # Mixing and matching files and inline text is supported.
    #   eg. read_text(['file1.txt', 'some text', 'file2.txt'])
    # Returns a string containing the concatenated contents of all text sources.
    def read_text(text_sources)
        raise "Text sources must be passed inside an Array" unless text_sources.class == Array
        raise "All text sources must be Strings" if text_sources.map(&:class).uniq != [String]
        raw_text = ""
        text_sources.each do |text_source|
            if File.file?(text_source)
                File.foreach(text_source) {|line| raw_text << line }
            else
                raw_text << text_source 
            end
            # Ensure there is at least one space between text sources.
            raw_text << " "
        end
        raw_text
    end

end
