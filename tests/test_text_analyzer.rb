require_relative '../text_analyzer'
require 'test/unit'

class TestTextAnalyzer < Test::Unit::TestCase

    def test_read_text_requires_proper_format
        assert_raise_message("All text sources must be Strings") do
            TextAnalyzer.new(['text/frankenstein.txt', 'text/leviathan.txt', 'Just some words!', 5])
        end

        assert_raise_message("Text sources must be passed inside an Array") do
            TextAnalyzer.new('text/frankenstein.txt')
        end
    end
    
    def test_read_text_will_treat_non_existant_files_as_text
        ta = TextAnalyzer.new(['text/where_am_i.txt', 'text/whoops_not_here.txt', 'Just some words!'])
        assert_equal(ta.raw_text, "text/where_am_i.txt text/whoops_not_here.txt Just some words! ")
    end

    def test_read_text_will_concatenate_all_text_sources
        one_text = TextAnalyzer.new(['text/frankenstein.txt'])
        two_texts = TextAnalyzer.new(['text/frankenstein.txt', 'Just some words!'])
        three_texts = TextAnalyzer.new(['text/frankenstein.txt', 'text/leviathan.txt', 'Just some words!'])
        assert_equal(one_text.raw_text.size, 420645)
        assert_equal(two_texts.raw_text.size, 420662)
        assert_equal(three_texts.raw_text.size, 1633177)
    end

    def test_get_words_only_returns_lowercase_words
        assert_equal(TextAnalyzer.new(['ALL CAPS', 'Uppercase Words', 'lowercase words']).get_words, ["all", "caps", "uppercase", "words", "lowercase", "words"])
    end
    
    def test_get_words_preserves_contractions_and_hyphenated_words
        assert_equal(TextAnalyzer.new(["Don't", "word-of-mouth"]).get_words, ["don't", "word-of-mouth"])
        assert_equal(TextAnalyzer.new(["Don’t"]).get_words, ["don’t"])
    end

    def test_get_words_does_not_include_any_spaces
        words = TextAnalyzer.new(['text/frankenstein.txt', 'text/leviathan.txt', 'Just some words!']).get_words
        assert_equal(words.map {|word| word.match(/\s/) }.flatten.compact.length, 0)
    end

    def test_get_words_honors_frankenstein
        assert_equal(TextAnalyzer.new(['which the dæmon']).get_words,['which', 'the', 'dæmon'])
    end

    def test_count_phrases_returns_proper_format
        ta = TextAnalyzer.new(['text/frankenstein.txt', 'text/leviathan.txt', 'Just some words!'])
        counted_phrases = ta.count_phrases
        assert_equal(counted_phrases.class, Hash)
        assert_equal(counted_phrases.keys.map(&:class).uniq, [String])
        assert_equal(counted_phrases.values.map(&:class).uniq, [Integer])
        assert(counted_phrases.size < ta.get_words.size)
    end

    def test_get_top_phrases_sorts_phrases_properly
        ta = TextAnalyzer.new(['text/frankenstein.txt', 'text/leviathan.txt', 'Just some words!'])
        counted_phrases = ta.count_phrases
        top_phrases = ta.get_top_phrases
        # Ensure the sum of the top 100 sorted counts is greater than the sum of any other 100 counts.
        assert(top_phrases.map { |arr| arr[1] }.flatten.inject(:+) > counted_phrases.values.sample(100).inject(:+))
        # Ensure the top 100 phrases are unique as they as passed to puts in an Array.
        assert_equal(top_phrases.size, top_phrases.map { |arr| arr[0] }.flatten.size)
    end
 
end
