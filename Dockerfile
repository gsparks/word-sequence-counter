FROM ruby:2.6

ENV LANG C.UTF-8

WORKDIR /usr/src/app

COPY . .

ENTRYPOINT ["ruby", "solution.rb"]

CMD []
