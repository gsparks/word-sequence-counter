#!/usr/bin/env ruby

require_relative 'text_analyzer'

if ARGV.length > 0
    TextAnalyzer.new(ARGV).print_top_phrases
else
    TextAnalyzer.new([STDIN.read]).print_top_phrases
end
