module PhraseHelper

    def get_top_phrases
        count_phrases.sort_by { |phrase, count| count }.last(100).reverse
    end

    # Iterate through Array of words in a linear fashion O(n) 
    #   to construct each phrase(3 words [O(1) + O(1) + O(1)]) and insert it into a Hash O(1)
    #   for overall run time of O(n)
    def count_phrases
        words = get_words
        boundary = words.size - 2
        phrases = Hash.new(0)
        words.each_index do |i|
            break if i == boundary
            phrase = [words[i], words[i+1], words[i+2]].join(' ')
            phrases[phrase] += 1
        end
        phrases
    end

    # Returns an Array of lowercase words without punctuation
    # Preserves apostrophes inside contractions and hyphenated words
    def get_words
        raw_text.downcase.scan /(?<=^|[^a-z])[a-z](?:[a-z'\-’æ\d]*[a-z])?/
    end

end
